package hr.dknez.bookingadmin.service;

import hr.dknez.bookingadmin.model.Booking;
import hr.dknez.bookingadmin.repository.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookingService {

   private BookingRepository bookingRepository;

   @Autowired
   public BookingService(BookingRepository bookingRepository) {
      this.bookingRepository = bookingRepository;
   }

   public List<Booking> getAllBookings() {
      return bookingRepository.findAll();
   }

   public Booking addBooking(Booking booking) {
      return bookingRepository.save(booking);
   }
}
