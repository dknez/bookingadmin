package hr.dknez.bookingadmin.service;

import hr.dknez.bookingadmin.model.Listing;
import hr.dknez.bookingadmin.repository.ListingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ListingService {

   private ListingRepository listingRepository;

   @Autowired
   public ListingService(ListingRepository listingRepository) {
      this.listingRepository = listingRepository;
   }

   public List<Listing> getAllListings() {
      return listingRepository.findAll();
   }
}
