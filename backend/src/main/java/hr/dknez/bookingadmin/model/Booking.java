package hr.dknez.bookingadmin.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Getter
@Setter
public class Booking {

   @Id
   @GeneratedValue(strategy = GenerationType.SEQUENCE)
   private Long id;

   private String guestName;

   private Integer numberOfGuests;

   @Temporal(TemporalType.DATE)
   private Date dateFrom;

   @Temporal(TemporalType.DATE)
   private Date dateTo;

   private Double nightlyPrice;

   private Double totalPrice;
}
