package hr.dknez.bookingadmin.controller;

import hr.dknez.bookingadmin.model.Booking;
import hr.dknez.bookingadmin.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping(value = "api/v1/booking")
@ResponseBody
public class BookingController {

   private final BookingService bookingService;

   @Autowired
   public BookingController(BookingService bookingService) {
      this.bookingService = bookingService;
   }

   @GetMapping
   public List<Booking> getAllBookings() {
      return bookingService.getAllBookings();
   }

   @PostMapping
   public Booking addBooking(@Valid @NotNull @RequestBody final Booking booking) {
      return bookingService.addBooking(booking);
   }
}
