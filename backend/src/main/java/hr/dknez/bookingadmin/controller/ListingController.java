package hr.dknez.bookingadmin.controller;

import hr.dknez.bookingadmin.model.Listing;
import hr.dknez.bookingadmin.service.ListingService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "api/v1/listing")
@ResponseBody
public class ListingController {

   private final ListingService listingService;


   public ListingController(ListingService listingService) {
      this.listingService = listingService;
   }

   @GetMapping
   public List<Listing> getAllListings() {
      return listingService.getAllListings();
   }
}
