import React from 'react';
import './App.css';
import NavBar from './component/NavBar'
import BookingList from './component/BookingList'

function App() {
  return (
    <div className="App">
      <NavBar />
      <BookingList />
    </div>
  );
}

export default App;
