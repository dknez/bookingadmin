import React, { Component } from 'react'
import axios from 'axios'
import { Grid } from '@material-ui/core';
import Booking from './Booking'

class BookingList extends Component {

    componentDidMount() {
        axios.get('/api/v1/booking')
            .then(res => {
                this.setState({ bookings: res.data })
                console.log(res.data)
            })
    }

    state = {
        bookings: []
    }

    render() {
        return (
            <div>
                <Booking bookings = {this.state.bookings} />
            </div>
        )
    }
}

export default BookingList