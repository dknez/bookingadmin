import React, {Component} from 'react'
import PropTypes from "prop-types";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { TableContainer } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';

class Booking extends Component {
    render() {
        return(
            <TableContainer component={Paper}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell>Guest name</TableCell>
                            <TableCell>Date from</TableCell>
                            <TableCell>Date to</TableCell>
                            <TableCell>Nightly price</TableCell>
                            <TableCell>Total price</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                    {this.props.bookings.map(booking => (
                        <TableRow key={booking.id}>
                            <TableCell component="th" scope="row">{booking.id}</TableCell>
                            <TableCell>{booking.guestName}</TableCell>
                            <TableCell>{booking.dateFrom}</TableCell>
                            <TableCell>{booking.dateTo}</TableCell>
                            <TableCell>{booking.nightlyPrice}</TableCell>
                            <TableCell>{booking.totalPrice}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
        );
    }
}

Booking.propTypes = {
    bookings: PropTypes.array.isRequired
}

export default Booking